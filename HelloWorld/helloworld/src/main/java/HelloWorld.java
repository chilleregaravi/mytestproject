package helloworld.src.main.java.helloworld;

import io.javalin.Javalin;

public class HelloWorld {
  public static void main(final String[] args) {
    Javalin app = Javalin.create().start(7000);
    app.get("/", ctx -> ctx.result("Hello World"));
  }
}
